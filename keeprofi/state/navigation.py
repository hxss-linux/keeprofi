
import subprocess
import re

import keeprofi

class Navigation():

	_process = None
	__selection = None
	__ls = None

	def _new_ls(self):
		raise NotImplementedError

	def _ls_str(self):
		raise NotImplementedError

	def _show_rofi(self):
		self._process = subprocess.run(
			[
				'rofi',
				'-dmenu',
				'-no-custom',
				'-format',
				'i',
				'-p',
				self._rofi_prompt,
			] + self._keybindings,
			input = self._ls_str(),
			text = True,
			capture_output = True
		)

		return self._process

	@property
	def _rofi_prompt(self):
		pass

	@property
	def _keybindings(self):
		pass

	@property
	def _selection(self):
		if (not self.__selection and self._process):
			i = int(self._process.stdout)
			self.__selection = self._ls[i]

		return self.__selection

	@property
	def _ls(self):
		if (not self.__ls):
			self.__ls = self._new_ls()

		return self.__ls

	def _free_kb(self, kb):
		result = []

		r = subprocess.run(
			'rofi -dump-xresources | grep ' + kb,
			shell = True,
			text = True,
			capture_output = True
		)

		if (r.stdout):
			bind = re.compile(r'^! rofi.(.+):\s+(.+)$')
			(name, keys) = bind.match(r.stdout).groups()
			keys = keys.split(',')
			keys.remove(kb)

			result = ['-' + name, ','.join(keys)]

		return result

	@property
	def _config(self):
		return keeprofi.config

	@property
	def _cache(self):
		return keeprofi.cache

	@property
	def _up_str(self):
		return '..'


