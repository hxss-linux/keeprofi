
from pathlib import PosixPath

import keeprofi

from . import State
from . import KPOpen
from . import FSNavigation

class Init(State):

	def handle(self) -> State:
		return (kdb := keeprofi.cache.get('last_file')) \
			and KPOpen(PosixPath(kdb)) \
			or FSNavigation()
