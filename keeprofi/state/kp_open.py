
import subprocess
from pathlib import PosixPath

from pykeepass import PyKeePass
from hxss.responsibility import chain

import keeprofi

from keeprofi import ExitHandler

from . import State
from . import FSNavigation
from . import KPNavigation

class KPOpen(State):

	_up_str = '..'

	__keyring = keeprofi.Keyring()

	def __init__(self, path: PosixPath):
		self._path = path.resolve()

	def handle(self) -> State:
		return self._connect_by_keyring() \
			or self._connect_by_user()

	def _connect_by_keyring(self) -> State:
		password = self.__keyring.get_pass()

		return password and self._connect(password) or None

	def _connect_by_user(self) -> State:
		process = self._show_rofi()

		return chain(process) \
			| ExitHandler() \
			| self._handle_up \
			| self._handle_pass \
			| chain

	def _handle_up(self, process) -> State:
		if (process.stdout == self._up_str):
			return FSNavigation(self._path.parent)

	def _handle_pass(self, process) -> State:
		connection = self._connect(process.stdout)

		if (not connection):
			self._wrong_password()

		return connection

	def _connect(self, password) -> State:
		result = None

		try:
			connection = PyKeePass(
				self._path.as_posix(),
				password=password
			)

			self.__keyring.save_pass(password)
			self._cache.last_file = self._path.as_posix()

			result = KPNavigation(connection)
		except Exception as e:
			pass

		return result

	def _show_rofi(self):
		process = subprocess.run(
			[
				'rofi',
				'-dmenu',
				'-password',
				'-p',
				'pass:',
			],
			input = self._up_str,
			text = True,
			capture_output = True
		)
		process.stdout = process.stdout.strip()

		return process

	@property
	def _selection(self):
		return self._process.stdout.strip()

	def _wrong_password(self):
		keeprofi.Notify(
			'Wrong password',
			self._path.as_posix(),
			self._config.icons['fail']
		) \
			.show()

	@property
	def _config(self):
		return keeprofi.config

	@property
	def _cache(self):
		return keeprofi.cache
