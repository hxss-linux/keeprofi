
from pathlib import PosixPath

import xerox
from pykeepass import PyKeePass
from pykeepass.group import Group
from pykeepass.entry import Entry
from pynput.keyboard import Controller
from hxss.responsibility import chain

import keeprofi

from keeprofi import ExitHandler

from . import State
from . import Navigation
from . import FSNavigation

class KPNavigation(Navigation, State):

	def __init__(self, kp: PyKeePass, group: Group = None):
		self._kp = kp
		self._group = group or self._kp.root_group

	def handle(self):
		process = self._show_rofi()

		return chain(process) \
			| ExitHandler() \
			| self._handle_up \
			| self._handle_chgroup \
			| self._handle_action \
			| chain

	def _handle_up(self, process):
		if (self._selection == self._group):
			delattr(self._cache, 'last_file')

			return FSNavigation(
				PosixPath(self._kp.filename).parent
			)

	def _handle_chgroup(self, process):
		if (isinstance(self._selection, Group)):
			return KPNavigation(self._kp, self._selection)

	def _handle_action(self, process):
		return {
			0:  self._default_action,
			10: self._custom_action,
			11: self._navigate_entry,
		}[process.returncode]()

	def _default_action(self):
		return (
			self._type_pass,
			self._copy_pass,
		)[self._config.is_copy_default()]()

	def _custom_action(self):
		return (
			self._copy_pass,
			self._type_pass,
		)[self._config.is_copy_default()]()

	def _copy_pass(self):
		entry = self._selection

		xerox.copy(entry.password)
		keeprofi.Notify(
			'Password copied',
			self._entry_title(entry),
			self._config.icons['success']
		).show()

		return None

	def _type_pass(self):
		Controller().type(self._selection.password)

		return None

	def _navigate_entry(self):
		from . import KPEntryNavigation

		return KPEntryNavigation(self._kp, self._selection)

	def _new_ls(self):
		return [self._parentgroup()] \
			+ sorted(
				self._group.subgroups,
				key = lambda g: g.name
			) \
			+ sorted(
				self._group.entries,
				key = self._entry_title
			)

	def _parentgroup(self):
		group = self._group.parentgroup or self._group
		group.name = self._up_str

		return group

	def _ls_str(self):
		return '\n'.join(map(
			self._item2str,
			self._ls
		))

	def _item2str(self, item):
		return isinstance(item, Group) \
			and '/' + item.name \
			or self._entry_title(item)

	def _entry_title(self, entry):
		return entry.title \
			or entry.username \
			or entry.url \
			or 'Entry#' + entry.uuid

	@property
	def _keybindings(self):
		if (not hasattr(self, '__keybindings')):
			self.__keybindings = [] \
				+ self._free_kb(self._kb_custom) \
				+ self._free_kb(self._kb_attrs) \
				+ ['-kb-custom-1', self._kb_custom] \
				+ ['-kb-custom-2', self._kb_attrs]

		return self.__keybindings

	@property
	def _kb_custom(self):
		return self._config.kb['custom_action']

	@property
	def _kb_attrs(self):
		return self._config.kb['pass_attrs']

	@property
	def _rofi_prompt(self):
		return 'kbd:'
