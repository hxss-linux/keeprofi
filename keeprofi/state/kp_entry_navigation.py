
import xerox
from pykeepass import PyKeePass
from pykeepass.entry import Entry
from pynput.keyboard import Controller
from hxss.responsibility import chain

import keeprofi

from keeprofi import ExitHandler

from . import State
from . import Navigation
from . import KPNavigation

class KPEntryNavigation(Navigation, State):

	def __init__(self, kp: PyKeePass, entry: Entry):
		self._kp = kp
		self._entry = entry

	def handle(self):
		process = self._show_rofi()

		return chain(process) \
			| ExitHandler() \
			| self._handle_up \
			| self._handle_action \
			| chain

	def _handle_exit(self, process):
		return process.returncode == 1 or None

	def _handle_up(self, process):
		if (self._selection == self._up_str):
			return KPNavigation(self._kp, self._entry.group)

	def _handle_action(self, process):
		return {
			0:  self._default_action,
			10: self._custom_action,
		}[process.returncode]()

	def _default_action(self):
		return (
			self._type_pass,
			self._copy_pass,
		)[self._config.is_copy_default()]()

	def _custom_action(self):
		return (
			self._copy_pass,
			self._type_pass,
		)[self._config.is_copy_default()]()

	def _copy_pass(self):
		xerox.copy(self._selection_value)
		keeprofi.Notify(
			'Password attribute copied',
			self._entry.title + '.' + self._selection,
			self._config.icons['success']
		).show()

		return None

	def _type_pass(self):
		Controller().type(self._selection_value)

		return None

	@property
	def _selection_value(self):
		return hasattr(self._entry, self._selection) \
			and getattr(self._entry, self._selection) \
			or self._entry.custom_properties[self._selection]

	def _new_ls(self):
		return [self._up_str] \
			+ list(filter(
				lambda attr: getattr(self._entry, attr),
				[
					'password',
					'username',
					'url',
					'notes',
					'tags',
				]
			)) \
			+ list(self._entry.custom_properties.keys())

	def _ls_str(self):
		return '\n'.join(self._ls)

	@property
	def _rofi_prompt(self):
		return 'entry:'

	@property
	def _keybindings(self):
		if (not hasattr(self, '__keybindings')):
			self.__keybindings = [] \
				+ self._free_kb(self._kb_custom) \
				+ ['-kb-custom-1', self._kb_custom]

		return self.__keybindings

	@property
	def _kb_custom(self):
		return self._config.kb['custom_action']

	@property
	def _up_str(self):
		return '/..'
