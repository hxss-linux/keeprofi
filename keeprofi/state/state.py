
class State():

	def handle(self) -> 'State':
		raise NotImplementedError
