
from pathlib import PosixPath
import subprocess

from hxss.responsibility import chain

import keeprofi

from keeprofi import ExitHandler

from . import State
from . import Navigation

class FSNavigation(Navigation, State):

	def __init__(self, cwd: PosixPath = None):
		self._cwd = cwd or PosixPath.home()

	def handle(self):
		process = self._show_rofi()

		return chain(process) \
			| ExitHandler() \
			| self._handle_switch_hidden \
			| self._handle_chdir \
			| self._handle_kp \
			| chain

	def _handle_switch_hidden(self, process):
		if (process.returncode == 10):
			self._switch_hidden()

			return FSNavigation(self._cwd)

	def _handle_chdir(self, process):
		if (self._selection.is_dir()):
			return FSNavigation(self._selection)

	def _handle_kp(self, process):
		return keeprofi.state.KPOpen(self._selection)

	def _new_ls(self):
		return [self._cwd.joinpath(self._up_str)] \
			+ list(
				filter(
					self._filter_files,
					sorted(
						self._cwd.iterdir(),
						key = self._sort_key
					)
				)
			)

	def _sort_key(self, item):
		return str(1 * item.is_file()) + item.name

	def _filter_files(self, item):
		return item.is_dir() \
			and (
				self._show_hidden
				or not item.name.startswith('.')
			) \
			or item.name.endswith('.kdbx')

	def _ls_str(self):
		return '\n'.join(map(
			self._item2str,
			self._ls
		))

	def _item2str(self, item):
		return self._render_dir(item) or item.name

	def _render_dir(self, item):
		return item.is_dir() \
			and self._config.dir_format \
				.format(name = item.name) \
			or None

	def _switch_hidden(self):
		self._show_hidden = not self._show_hidden

	@property
	def _show_hidden(self):
		return self._cache.show_hidden_files

	@_show_hidden.setter
	def _show_hidden(self, show_hidden):
		self._cache.show_hidden_files = show_hidden

	@property
	def _rofi_prompt(self):
		return 'file:'

	@property
	def _keybindings(self):
		if (not hasattr(self, '__keybindings')):
			self.__keybindings = [] \
				+ self._free_kb(self._config.kb['hidden']) \
				+ ['-kb-custom-1', self._config.kb['hidden']]

		return self.__keybindings
