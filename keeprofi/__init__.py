
APP_NAME = __name__

from .logger import logger

def _init_config():
	from .resource import Config

	return Config()

def _init_cache():
	from .resource import Cache

	return Cache()

config = _init_config()
cache = _init_cache()

from .notify import Notify
from .keyring import Keyring

from .exit_handler import ExitHandler

from .state import Init

def main():
	state = Init()

	while state:
		state = state.handle()
