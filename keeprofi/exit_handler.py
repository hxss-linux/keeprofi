
import subprocess

from hxss.responsibility import Link

class ExitHandler(Link):

	def __init__(self):
		pass

	def handle(self, process: subprocess.CompletedProcess):
		if (process.returncode == 1):
			return self._response(None)
